const windowParameters = { x_wmax: 80, y_wmax: 80, x_wmin: 20, y_wmin: 40 };
const viewportParameters = { x_vmax: 60, y_vmax: 60, x_vmin: 30, y_vmin: 40 };
const pointOnWindow = { x_w: 30, y_w: 80 };

function WindowtoViewport(windowParameters, viewportParameters, pointOnWindow) {
  const { x_wmax, y_wmax, x_wmin, y_wmin } = windowParameters;
  const { x_vmax, y_vmax, x_vmin, y_vmin } = viewportParameters;
  const { x_w, y_w } = pointOnWindow;

  let sx, sy;
  sx = (x_vmax - x_vmin) / (x_wmax - x_wmin);
  sy = (y_vmax - y_vmin) / (y_wmax - y_wmin);

  let x_v, y_v;
  x_v = x_vmin + (x_w - x_wmin) * sx;
  y_v = y_vmin + (y_w - y_wmin) * sy;
  console.log("The point on viewport: (%d, %d )\n ", x_v, y_v);
}

WindowtoViewport(windowParameters, viewportParameters, pointOnWindow);
