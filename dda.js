function DDA(startCoordinate, endCoordinate) {
  const x1 = startCoordinate.x;
  const y1 = startCoordinate.y;
  const x2 = endCoordinate.x;
  const y2 = endCoordinate.y;

  // calculate dx & dy
  let dx = Math.abs(x2 - x1);
  let dy = Math.abs(y2 - y1);

  // calculate steps required for generating pixels
  let steps = Math.max(dx, dy);

  // calculate increment in x & y for each steps
  let Xinc = dx / steps;
  let Yinc = dy / steps;

  // Put pixel for each step
  let X = x1;
  let Y = y1;
  for (let i = 0; i <= steps; i++) {
    console.log(X, Y); // put pixel at (X,Y)
    X += Xinc; // increment in x at each step
    Y += Yinc; // increment in y at each step
    // delay(100); // for visualization of line-
    // generation step by step
  }
}

DDA({ x: 2, y: 2 }, { x: 14, y: 16 });
