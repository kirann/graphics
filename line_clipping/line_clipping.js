const INSIDE = 0; // 0000
const LEFT = 1; // 0001
const RIGHT = 2; // 0010
const BOTTOM = 4; // 0100
const TOP = 8; // 1000
const x_max = 200;
const y_max = 160;
const x_min = 80;
const y_min = 80;

function drawRect(x1, y1, x2, y2, color = "#000000") {
  drawLine({ x: x1, y: y1 }, { x: x2, y: y1 }, color);
  drawLine({ x: x1, y: y1 }, { x: x1, y: y2 }, color);
  drawLine({ x: x2, y: y2 }, { x: x2, y: y1 }, color);
  drawLine({ x: x2, y: y2 }, { x: x1, y: y2 }, color);
}
drawRect(x_min, y_min, x_max, y_max, "#FF0000");

function computeCode(x, y) {
  let code = INSIDE;
  if (x < x_min) code |= LEFT;
  else if (x > x_max) code |= RIGHT;
  if (y < y_min) code |= BOTTOM;
  else if (y > y_max) code |= TOP;
  return code;
}

function cohenSutherlandClip(x1, y1, x2, y2) {
  let code1 = computeCode(x1, y1);
  let code2 = computeCode(x2, y2);
  let accept = false;

  while (true) {
    if (code1 == 0 && code2 == 0) {
      accept = true;
      break;
    } else if (code1 & code2) {
      break;
    } else {
      let code_out;
      let x, y;

      if (code1 != 0) code_out = code1;
      else code_out = code2;

      if (code_out & TOP) {
        x = x1 + ((x2 - x1) * (y_max - y1)) / (y2 - y1);
        y = y_max;
      } else if (code_out & BOTTOM) {
        x = x1 + ((x2 - x1) * (y_min - y1)) / (y2 - y1);
        y = y_min;
      } else if (code_out & RIGHT) {
        y = y1 + ((y2 - y1) * (x_max - x1)) / (x2 - x1);
        x = x_max;
      } else if (code_out & LEFT) {
        y = y1 + ((y2 - y1) * (x_min - x1)) / (x2 - x1);
        x = x_min;
      }

      if (code_out == code1) {
        x1 = x;
        y1 = y;
        code1 = computeCode(x1, y1);
      } else {
        x2 = x;
        y2 = y;
        code2 = computeCode(x2, y2);
      }
    }
  }
  if (accept)
    return {
      x1,
      y1,
      x2,
      y2
    };

  return false;
}

let result = cohenSutherlandClip(100, 100, 140, 140);
console.log("Test Line 1\n");
console.log({ x1: 100, y1: 100, x2: 140, y2: 140 });
drawLine({ x: 100, y: 100 }, { x: 140, y: 140 }, "#CCCCCC");
if (result)
  drawLine({ x: result.x1, y: result.y1 }, { x: result.x2, y: result.y2 });
console.log("Test Line 1 Result\n", result);

result = cohenSutherlandClip(140, 180, 220, 80);
console.log("Test Line 2\n");
console.log({ x1: 140, y1: 180, x2: 220, y2: 80 });

drawLine({ x: 140, y: 180 }, { x: 220, y: 80 }, "#CCCCCC");
if (result)
  drawLine({ x: result.x1, y: result.y1 }, { x: result.x2, y: result.y2 });
console.log("Test Line 2 Result\n", result);

result = cohenSutherlandClip(20, 100, 100, 20);
console.log("Test Line 3\n");
console.log({ x1: 20, y1: 100, x2: 100, y2: 20 });
drawLine({ x: 20, y: 100 }, { x: 100, y: 20 }, "#CCCCCC");
if (result)
  drawLine({ x: result.x1, y: result.y1 }, { x: result.x2, y: result.y2 });
console.log("Test Line 3 Result\n Line is rejected");
