function shearx_matrix(shx) {
  return [
    [1, shx, 0],
    [0, 1, 0],
    [0, 0, 1]
  ];
}

function sheary_matrix(shy) {
  return [
    [1, 0, 0],
    [shy, 1, 0],
    [0, 0, 1]
  ];
}

const msx = shearx_matrix(4);
const msy = sheary_matrix(2);

const shearx_triangle = transformTriangle(triangle, msx);
const sheary_triangle = transformTriangle(triangle, msy);

// drawing orginal triangle
console.log("Triangle\n", triangle);
drawLine(triangle[0], triangle[1]);
drawLine(triangle[1], triangle[2]);
drawLine(triangle[0], triangle[2]);

//draw shearx triangle
console.log("Sheared on X-axis\n", triangle);
drawLine(shearx_triangle[0], shearx_triangle[1]);
drawLine(shearx_triangle[1], shearx_triangle[2]);
drawLine(shearx_triangle[0], shearx_triangle[2]);

//draw sheary triangle
console.log("Sheared on Y-axis\n", triangle);
drawLine(sheary_triangle[0], sheary_triangle[1]);
drawLine(sheary_triangle[1], sheary_triangle[2]);
drawLine(sheary_triangle[0], sheary_triangle[2]);
