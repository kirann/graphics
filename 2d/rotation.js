function rotate_matrix(theta) {
  theta = (Math.PI * theta) / 180;
  return [
    [Math.cos(theta), -Math.sin(theta), 0],
    [Math.sin(theta), Math.cos(theta), 0],
    [0, 0, 1]
  ];
}

const mr = rotate_matrix(90);
const rotated_triangle = transformTriangle(triangle, mr);

// drawing orginal triangle
console.log("Triangle\n", triangle);
drawLine(triangle[0], triangle[1]);
drawLine(triangle[1], triangle[2]);
drawLine(triangle[0], triangle[2]);

//draw rotated triangle
console.log("Rotated Triangle\n", rotated_triangle);
drawLine(rotated_triangle[0], rotated_triangle[1]);
drawLine(rotated_triangle[1], rotated_triangle[2]);
drawLine(rotated_triangle[0], rotated_triangle[2]);
