function translate_matrix(tx, ty) {
  return [
    [1, 0, tx],
    [0, 1, ty],
    [0, 0, 1]
  ];
}

const translateMatrix = translate_matrix(40, 40);
const translatedTriangle = transformTriangle(triangle, translateMatrix);

// drawing orginal triangle
console.log("Triangle\n", triangle);
drawLine(triangle[0], triangle[1]);
drawLine(triangle[1], triangle[2]);
drawLine(triangle[0], triangle[2]);

//draw translated triangle
console.log("Translated Triangle\n", translatedTriangle);
drawLine(translatedTriangle[0], translatedTriangle[1]);
drawLine(translatedTriangle[1], translatedTriangle[2]);
drawLine(translatedTriangle[0], translatedTriangle[2]);
