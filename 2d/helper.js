function matrix_from_point(x, y) {
  return [[x], [y], [1]];
}

function multiply_two_matrix(m, p) {
  const x = m[0][0] * p[0] + m[0][1] * p[1] + m[0][2] * p[2];
  const y = m[1][0] * p[0] + m[1][1] * p[1] + m[1][2] * p[2];

  return { x, y };
}

const triangle = [
  { x: 20, y: 30 },
  { x: 40, y: 50 },
  { x: 60, y: 30 }
];

function transformTriangle(triangle, matrix) {
  var newPoints = new Array();
  triangle.forEach(element => {
    newPoints.push(
      multiply_two_matrix(matrix, matrix_from_point(element.x, element.y))
    );
  });
  return newPoints;
}

// module.exports = {
//   triangle,
//   transformTriangle
// };
