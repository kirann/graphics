function scale_matrix(sx, sy) {
  return [
    [sx, 0, 0],
    [0, sy, 0],
    [0, 0, 1]
  ];
}

const scaleMatrix = scale_matrix(3, 3);
const scaled_triangle = transformTriangle(triangle, scaleMatrix);

console.log("Triangle\n", triangle);
// drawing orginal triangle
drawLine(triangle[0], triangle[1]);
drawLine(triangle[1], triangle[2]);
drawLine(triangle[0], triangle[2]);

//draw scaled triangle
console.log("Scaled Triangle\n", scaled_triangle);
drawLine(scaled_triangle[0], scaled_triangle[1]);
drawLine(scaled_triangle[1], scaled_triangle[2]);
drawLine(scaled_triangle[0], scaled_triangle[2]);
