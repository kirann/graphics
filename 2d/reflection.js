function rotate_matrix(theta) {
  theta = (Math.PI * theta) / 180;
  return [
    [Math.cos(theta), -Math.sin(theta), 0],
    [Math.sin(theta), Math.cos(theta), 0],
    [0, 0, 1]
  ];
}

const mr = rotate_matrix(180);

const reflected_triangle = transformTriangle(triangle, mr);

// drawing orginal triangle
console.log("Triangle\n", triangle);

drawLine(triangle[0], triangle[1]);
drawLine(triangle[1], triangle[2]);
drawLine(triangle[0], triangle[2]);

// draw reflected triangle
console.log("Reflection through origin\n", reflected_triangle);
drawLine(reflected_triangle[0], reflected_triangle[1]);
drawLine(reflected_triangle[1], reflected_triangle[2]);
drawLine(reflected_triangle[0], reflected_triangle[2]);
