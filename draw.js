//javascript code

//selecting canvas that is in html
var canvas = document.getElementById("canvas");
var canvasWidth = canvas.width;
var canvasHeight = canvas.height;
var ctx = canvas.getContext("2d");
ctx.translate(200, 200);
// Add x and y axis to the canvas
ctx.beginPath();
ctx.moveTo(-200, 0);
ctx.lineTo(200, 0);
ctx.stroke();
ctx.beginPath();
ctx.moveTo(0, -200);
ctx.lineTo(0, 200);
ctx.stroke();
ctx.font = "12px Arial";
ctx.fillText("+ x", 180, -10);
ctx.fillText("- y", 10, 180);
ctx.fillText("- x", -200, -10);
ctx.fillText("+ y", 10, -180);

// function to draw cartesian coordinate in canvas
function drawCartesianPoint(x, y) {
  ctx.fillRect(x, -y, 2, 2);
}

const drawLine = (start, end, color = "#000000") => {
  ctx.beginPath();
  ctx.strokeStyle = color;
  ctx.moveTo(start.x, -start.y);
  ctx.lineTo(end.x, -end.y);
  ctx.stroke();
};

const drawLineNegative = (start, end) => {
  ctx.beginPath();
  ctx.moveTo(start.x, start.y);
  ctx.lineTo(end.x, end.y);
  ctx.stroke();
};
