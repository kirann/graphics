function mid_point_ellipse(xc, yc, rx, ry) {
  let coordinatesArray = new Array();
  let dx, dy, d1, d2, x, y;
  x = 0;
  y = ry;
  d1 = ry * ry - rx * rx * ry + 0.25 * rx * rx;
  dx = 2 * ry * ry * x;
  dy = 2 * rx * rx * y;

  while (dx < dy) {
    coordinatesArray.push({ x: x + xc, y: y + yc });
    coordinatesArray.push({ x: -x + xc, y: y + yc });
    coordinatesArray.push({ x: x + xc, y: -y + yc });
    coordinatesArray.push({ x: -x + xc, y: -y + yc });
    if (d1 < 0) {
      x++;
      dx = dx + 2 * ry * ry;
      d1 = d1 + dx + ry * ry;
    } else {
      x++;
      y--;
      dx = dx + 2 * ry * ry;
      dy = dy - 2 * rx * rx;
      d1 = d1 + dx - dy + ry * ry;
    }
  }

  d2 =
    ry * ry * ((x + 0.5) * (x + 0.5)) +
    rx * rx * ((y - 1) * (y - 1)) -
    rx * rx * ry * ry;

  while (y >= 0) {
    coordinatesArray.push({ x: x + xc, y: y + yc });
    coordinatesArray.push({ x: -x + xc, y: y + yc });
    coordinatesArray.push({ x: x + xc, y: -y + yc });
    coordinatesArray.push({ x: -x + xc, y: -y + yc });

    if (d2 > 0) {
      y--;
      dy = dy - 2 * rx * rx;
      d2 = d2 + rx * rx - dy;
    } else {
      y--;
      x++;
      dx = dx + 2 * ry * ry;
      dy = dy - 2 * rx * rx;
      d2 = d2 + dx - dy + rx * rx;
    }
  }
  return coordinatesArray;
}

const ellipse = mid_point_ellipse(5, 5, 100, 50);
ellipse.forEach(item => {
  drawCartesianPoint(parseInt(item.x), parseInt(item.y));
});
