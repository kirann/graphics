function mid_point_circle(xc, yc, r) {
  var coordinatesArray = new Array();
  // initial decision paramenter
  var p = 1 - r;
  var x = 0;
  var y = r;

  while (x <= y) {
    // adding coordinate for all segments
    coordinatesArray.push({ x: x + xc, y: y + yc });
    coordinatesArray.push({ x: x + xc, y: -y + yc });
    coordinatesArray.push({ x: -x + xc, y: y + yc });
    coordinatesArray.push({ x: -x + xc, y: -y + yc });
    coordinatesArray.push({ x: y + yc, y: x + xc });
    coordinatesArray.push({ x: y + yc, y: -x + xc });
    coordinatesArray.push({ x: -y + yc, y: x + xc });
    coordinatesArray.push({ x: -y + yc, y: -x + xc });

    x++;
    if (p < 0) {
      p = p + 2 * x + 1;
    } else {
      y--;
      p = p + 2 * x - 2 * y + 1;
    }
  }

  return coordinatesArray;
}

const circle = mid_point_circle(50, 50, 100);
console.log(circle);
