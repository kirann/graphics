function bsa_line(startCoordinates, endCoordinates) {
  let coordinatesArray = new Array();
  let x1 = startCoordinates.x;
  let y1 = startCoordinates.y;
  let x2 = endCoordinates.x;
  let y2 = endCoordinates.y;
  let dx = Math.abs(x2 - x1);
  let dy = Math.abs(y2 - y1);

  const slope_value = dx >= dy; //determine m>1 or m<=1
  let steps = slope_value ? parseInt(dx) + 1 : parseInt(dy) + 1; // calculate max loop steps required

  console.log("\nCalculating for M", slope_value ? "<=1" : ">1");
  let p = slope_value ? 2 * dy - dx : 2 * dx - dy;
  while (steps) {
    coordinatesArray.push({ x: x1, y: y1 });
    steps--;
    slope_value ? x1++ : y1++;
    if (p < 0) {
      p = slope_value ? p + 2 * dy : p + 2 * dx;
    } else {
      p = slope_value ? p + 2 * dy - 2 * dx : p + 2 * dx - 2 * dy;
      slope_value ? y1++ : x1++;
    }
  }

  return coordinatesArray;
}

console.log(bsa_line({ x: 3, y: 2 }, { x: 6, y: 5 }));
