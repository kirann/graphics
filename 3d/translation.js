const { getTransformedPoint, getPointMatrix } = require("./helper");

function getTranslationMatrix(translationFactors) {
  const { tx, ty, tz } = translationFactors;
  return [
    [1, 0, 0, 0],
    [0, 1, 0, 0],
    [0, 0, 1, 0],
    [tx, ty, tz, 1]
  ];
}

let translationFactors = { tx: 8, ty: 5, tz: 7 };
let point = { x: 40, y: 44, z: 35 };

console.log("\n Orginal Point\n", point);

console.log(
  "Translated Point: \n",
  getTransformedPoint(
    getTranslationMatrix(translationFactors),
    getPointMatrix(point)
  )
);
