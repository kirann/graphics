const { getTransformedPoint, getPointMatrix } = require("./helper");

function getScalingMatrix(translationFactors) {
  const { sx, sy, sz } = translationFactors;
  return [
    [sx, 0, 0, 0],
    [0, sy, 0, 0],
    [0, 0, sz, 0],
    [0, 0, 0, 1]
  ];
}

let scalingFactors = { sx: 8, sy: 5, sz: 7 };
let point = { x: 40, y: 44, z: 35 };
console.log("\n Orginal Point\n", point);
console.log(
  "Scaled Point: \n",
  getTransformedPoint(getScalingMatrix(scalingFactors), getPointMatrix(point))
);
