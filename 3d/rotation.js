const { getTransformedPoint, getPointMatrix } = require("./helper");

function getRotationMatrixOnX(theta) {
  theta = (Math.PI * theta) / 180;
  return [
    [1, 0, 0, 0],
    [0, Math.cos(theta), -Math.sin(theta), 0],
    [0, Math.sin(theta), Math.cos(theta), 0],
    [0, 0, 0, 1]
  ];
}

function getRotationMatrixOnY(theta) {
  theta = (Math.PI * theta) / 180;
  return [
    [Math.cos(theta), 0, Math.sin(theta), 0],
    [0, 1, 0, 0],
    [-Math.sin(theta), 0, Math.cos(theta), 0],
    [0, 0, 0, 1]
  ];
}

function getRotationMatrixOnZ(theta) {
  theta = (Math.PI * theta) / 180;
  return [
    [Math.cos(theta), -Math.sin(theta), 0, 0],
    [Math.sin(theta), Math.cos(theta), 0, 0],
    [0, 0, 1, 0],
    [0, 0, 0, 1]
  ];
}

let theta = 10;
let point = { x: 40, y: 44, z: 35 };

console.log("\nOriginal Point\n", point);

console.log(
  "\nRotation on X-axis \n",
  getTransformedPoint(getRotationMatrixOnX(theta), getPointMatrix(point))
);
console.log(
  "\nRotation on Y-axis \n",
  getTransformedPoint(getRotationMatrixOnY(theta), getPointMatrix(point))
);
console.log(
  "\nRotation on Z-axis \n",
  getTransformedPoint(getRotationMatrixOnZ(theta), getPointMatrix(point))
);
