function getTransformedPoint(transformationMatrix, point) {
  const x =
    point[0] * transformationMatrix[0][0] +
    point[1] * transformationMatrix[1][0] +
    point[2] * transformationMatrix[2][0] +
    point[3] * transformationMatrix[3][0];

  const y =
    point[0] * transformationMatrix[0][1] +
    point[1] * transformationMatrix[1][1] +
    point[2] * transformationMatrix[2][1] +
    point[3] * transformationMatrix[3][1];

  const z =
    point[0] * transformationMatrix[0][2] +
    point[1] * transformationMatrix[1][2] +
    point[2] * transformationMatrix[2][2] +
    point[3] * transformationMatrix[3][2];

  return { x, y, z };
}

function getPointMatrix(point) {
  const { x, y, z } = point;
  return [x, y, z, 1];
}

module.exports = { getTransformedPoint, getPointMatrix };
